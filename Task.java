import java.util.*;

class Task {
    int id, time, staff;
    String name;
    int earliestStart, latestStart;
    ArrayList<Task> inEdges;
    ArrayList<Task> outEdges;  // = new ArrayList();
    int cntPredecessors;
    

    public Task(int id) {
	this.id = id;
	this.outEdges = new ArrayList<Task>();
	this.inEdges = new ArrayList<Task>();
    }
    
    public void setName(String name) {
	this.name = name;
    }
    
    public void setTime(int time) {
	this.time = time;
    }
    
    public void setStaff(int staff) {
	this.staff = staff;
    }
    
    public void setInEdges(List<String> l) {
	
	for (int i=0; i < l.size(); i++) {
	    //if ( Integer.parseInt(l.get(i)) != 0 ) {
	    // System.out.print("inEdge is " + Integer.parseInt(l.get(i)) + " "); 
		inEdges.add(new Task(Integer.parseInt(l.get(i))));
	    
	}
    }
    public void setOutEdge(Task t) {

	System.out.print(" Adding " + t.id + " to " + this.id + " list of outEdges");
	this.outEdges.add(t);
    }
	

    public int getCntPredecessors () {
	return this.cntPredecessors;
    }

    /*public ListTask[] getDeps() {
	if (this.inEdges.get(0).id == 0) return null;
	return this.inEdges;
	
	}*/
    
    public void calculateIndegrees() {
	if (this.inEdges.get(0).id == 0) { 
	} else {
	    for(int i=0;i < this.inEdges.size();i++ ) {
		this.cntPredecessors++;
	    }
	}
    }
    
    @Override public boolean equals(Object other) {
	boolean result = false;
	if (other instanceof Task) {
	    Task that = (Task) other;
	    result = (this.id == that.id);
	}
	return result;
    }
    
    @Override public int hashCode() {
    return this.id;
    }
    

    
} // END CLASS TASK 




