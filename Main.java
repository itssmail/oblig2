import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Pattern;

public class Main {
    static int maxnr = 0;
    public static Task[] tasks;
    public static void main(String[] args) {
        File file = new File("buildhouse2.txt");
        Scanner projectScanner = null;
        try {
            projectScanner = new Scanner(file);
            /*while (wordscanner.hasNext()) {
              System.out.println(wordscanner.next());
              } */
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
	//String first = projectScanner.next();
	maxnr = projectScanner.nextInt();
	ArrayList<Task> tasksList = new ArrayList<Task>(maxnr+1);
	//System.out.println("Project tasks : " + maxnr ); 
	Task task = null;
	while (projectScanner.hasNextLine()) {
	    int pos = 0;
	    int[] inDegreesArr = null; 
	    String line1 = projectScanner.nextLine();
	    if (line1.isEmpty() ) continue;
	    //String[] detailarr =  line1.split("\\s+");
	    ArrayList<String> detailArrayList = new ArrayList<String>(Arrays.asList(line1.split("\\s+")));
	    List<String> inEdgesList = detailArrayList.subList(4, detailArrayList.size());
	    task = new Task(Integer.parseInt(detailArrayList.get(pos++)));
	    //	    System.out.print(" Curr Task is  " + task.id + "  ");
	    if (!tasksList.contains(task)) { // this task isn't in the taskList hence we add it
		tasksList.add(task);
		task.setInEdges(inEdgesList);
		task.setName(detailArrayList.get(pos++));
		task.setTime(Integer.parseInt(detailArrayList.get(pos++)));
		task.setStaff(Integer.parseInt(detailArrayList.get(pos++)));
		
		//try to set outEdges
		//for (int i=0; i < inEdgesList.size()-1; i++) {
		System.out.println( "\n");
		Iterator<String> iterator = inEdgesList.iterator();
		while (iterator.hasNext()) {
		    String nextItem =   iterator.next();
		    if (!nextItem.equals("0")){ 
			//System.out.print(" Iterator in inEdgeList returns:  " +  nextItem );
			Task currDepTask =  new Task(Integer.parseInt(nextItem));
			currDepTask.setOutEdge(task);
		
			//if (tasksList.contains(currDepTask))  System.out.print(" currDepTask has id and is in the list " + currDepTask.id );
			
			if (!tasksList.contains(currDepTask)) {
			    // System.out.print(" currDepTask has id " + currDepTask.id );
			    tasksList.add(currDepTask);
			    
			}
		    }
		}
	    } else { // find and update this task in tasksList
		//Task taskToUpdate = tasksList.get(task.id-1);
		int indexOfUpdateTask = tasksList.indexOf(task);
		System.out.println( "\nIn the ELSE to update " + task.id + " on index " + indexOfUpdateTask);
		Task updateTask = tasksList.get(indexOfUpdateTask); 
		updateTask.setInEdges(inEdgesList);
		updateTask.setName(detailArrayList.get(pos++));
		updateTask.setTime(Integer.parseInt(detailArrayList.get(pos++)));
		updateTask.setStaff(Integer.parseInt(detailArrayList.get(pos++)));
		//task.setOutEdges(inEdgesList);
		
	    }
	}   
	System.out.println("tasksLists has size: " + tasksList.size() + " " );
	System.out.println();
	//for(int i=0;i < tasksList.size();i++ ) {
        Iterator<Task> tasksListIterator = tasksList.iterator();
	while (tasksListIterator.hasNext()) {
	    //System.out.print("Task name is: " + tasks[i].name + " ");
	    Task nextTask = tasksListIterator.next();
	    System.out.println(" nextTask has value: " +  nextTask.id);
	    if (nextTask.id == 0) {
		
		    }
	    //nextTask.calculateIndegrees();
	    //System.out.println("InDegrees for Node " + nextTask.id + " is " +  nextTask.cntPredecessors);
	}
	System.out.println();
	//setOutDegrees(tasks);
	Iterator<Task> it = tasksList.iterator(); 
	while (it.hasNext()) {
	    Task tmpTask = it.next();
	    //System.out.print(" TmpId is " + tmpTask.name);
	    System.out.println(" outEdges count for " + tmpTask.id + " is "  + tmpTask.outEdges.size());
	}
	System.out.println(" ");
	
	
    } // END  public static void main()
    
} // END CLASS MAIN

